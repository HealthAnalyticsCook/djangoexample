from django.conf.urls import url
from django.contrib import admin
from django.views.generic import TemplateView
from . import views

urlpatterns = [
    url(r'^Locations/$', views.LocationList.as_view(), name='locations'),
    url(r'^AddLocation/$', views.locationForm, name='location_form'),
]

#    url(r'^$', TemplateView.as_view(template_name='index.html'), name='home'),
