from django.shortcuts import render
from django.views.generic import ListView
from django.http import HttpResponse, Http404, HttpResponseNotFound, HttpResponseRedirect

from .models import Locations
from .forms import LocationForm
# Create your views here.


class LocationList(ListView):
    model = Locations


def locationForm(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = LocationForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            form.save()
            # redirect to a new URL:
            return HttpResponseRedirect('/AddLocation/')

    # if a GET (or any other method) we'll create a blank form
    else:
        form = LocationForm()

    return render(request, 'form.html', {'form': form})

